# convert Diceware word list to JSON object

out = """/**
 * Hawke AI Random Roller
 * Dicware Words
 * Language: en
 */
{ """

dw = []
with open("diceware.wordlist.mod","r") as f:
    dw = f.readlines()

for w in dw:
    out = out + '"' + w[0:5] + '":"' + w[6:-1] + '", '

out = out + "}\n"
with open("src/diceware/dw-en.json","w",encoding="utf-8") as f:
    f.write(out);
