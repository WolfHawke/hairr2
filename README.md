# Hawke AI Random Roller
Version: 2.0

Visit [https://rr.hawke-ai.com](https://rr.hawke-ai.com) to see Random Roller in action.

## Installing
1. Download the or clone the repository and expose the files in the `src` folder to the internet.
2. To localize the interface to an existing langugage edit the `lang` attribute in the `<html>` tag of `index.html`.

## Localizing
The application can be localized by copying the `en.json` file in the `i18n` to the file with the language code you wish to translate it to (e.g. for French, copy `en.json` to `fr.json`). You can then edit the JSON. 

Please be careful to maintain the `<span>` and `<a>` tags in the various fields so the links will continue to work.

If a Diceware file exists in the language that you're localizing the interface to, you can find it on A.G. Reinhold's [Diceware Passphrase Home Page](https://theworld.com/~reinhold/diceware.html). You will need to download the file and convert it to a JSON file similar to `dw-en.json` in the `diceware` directory. Name it `dw-[langcode].json` (e.g. for French `dw-fr.json`) and place it in the `diceware` directory. If you do not create the Diceware word list file, Random Roller will use the English word list.

When you are finished, change the `<html lang="">` attribute to reflect the new language, reload the site and it should now work in the new language.

Please consider submitting your localizaiton to the repository so others can benefit from it.

## Submitting Issues
You can submit any issues you find along with suggested fixes to the Issues tracker. Because this is mostly maintained as a hobby project, the maintainer cannot promise to quickly implement any fixes to the repository and asks for your understanding.
