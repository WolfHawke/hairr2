/**
 * Hawke AI Random Roller
 * JavaScript interface functions
 *
 * Last update: 2021-09-19
 * 
 * @ver 2.0
 * @author JMW
 */

/**
 * Object: RRInterface
 * Description: object to run the Random Roller interface.
 * 
 * @since 2.0
 * 
 * @param   {string}    locale//#endregionTwo or five-character language code
 * @returns             nothing
 */
function RRInterface(locale) {
    if (typeof locale != "string" || (locale.length != 2 && locale.length != 5)) { locale = ""; }

    /* object properties */
    this.activeScreen = "welcome"; /* we are showing the welcome screen on activation */
    this.langCode = locale;   /* language code of localization */
    this.strings = {};        /* object containing strings */
    this.toTranslate = []     /* array of object IDs to translate */
    this._wait = false;


    /* object internal self referent */
    var _me = this;

    /* object methods */
    /**
     * Function: _init
     * Description: initialization function for the script; 
     *              loads internationalization, resizes viewport,
     *              links button actions,
     *              displays welcome screen
     *
     * @since: 2.0
     * 
     * @returns         nothing
     */
    this._init = function () {
        /* loading message */
        $("#rr_start_loading").show();

        /* initiate internationalization */
        /* get language code of html field if no language is defined (default behavior) */
        if (_me.langCode == "") { _me.langCode = $("html").attr("lang"); }

        /* load strings */
        _me._wait = true;
        $.getJSON("i18n/" + _me.langCode + ".json", function(d) { 
            _me.strings = d;
            _me.loadAllStrings();
        }).fail(function(d) {
            console.log("Unable to retrieve language strings", d);
            /* link about page */
            $(".rr-about").on("click", function () { _me.getAboutPg($(this).data("aboutpg")) });

            /* display welcome page */
            $("#rr_start").fadeOut();
        });

        /* load fields to translate */
        var xlationClasses = [".rr-i18n", ".rr-i18n-title"]
        for (var i=0; i < xlationClasses.length; i++){
            $(xlationClasses[i]).each( function (i, obj) {
                var randId = _me._rndString();
                if (obj.id == "") { obj.id = "rr-l18n-" + randId; }
                _me.toTranslate.push(obj.id);
            });
        }

        /* link buttons */
        $("#mode_dice").on("click", function() { _me.setMode("dice"); });
        $("#mode_dice").attr("title", "Switch to Dice Mode");
        $("#mode_auto").on("click", function() { _me.setMode("auto"); });
        $("#mode_auto").attr("title", "Switch to Automatic Mode");
        $("#welcome_mode_dice").on("click", function() { _me.setMode("dice"); });
        $("#welcome_mode_auto").on("click", function() { _me.setMode("auto"); });
        $("#copy_button_btn").click(function() { _me.copyToClipboard('#rr_pw', '#copy_button_btn'); });
        $("#update_rr").click(function () { window.location.reload(); });

        /* set version and update on about page */
        $("#rr_version").html($("meta[name=version]").attr("content"));
        $("#rr_update_date").html($("meta[name=updated]").attr("content"));

        
        /* set up page resizing */
        _me.viewportSize();
        window.onresize = _me.viewportSize;
    }

    /**
     * Function: _rndString
     * Description: creates a random string of alphanumeric characters in a given length
     * Arguments: l = length of string to generate; default = 7
     * Returns: string of given length
     * 
     * @since 2.0
     * 
     * @param   {integer}   l   length of string to generate; default=7
     * @returns {string}
     */
    this._rndString = function (l) {
        /* set default */
        if (typeof l != "integer") { l = 7; }

        /* characters */
        var c = [" ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "_"];

        /* random number placeholder */
        var r = 0;

        /* output string */
        var s = "";

        for (var i=0; i<l; i++) {
            r = -1;
            while (r < 0 || r > (c.length - 1)) {
                r = Math.round(Math.random() * 100);
            }
            s += c[r];
        }
        return s;
    }

    /** 
     * Function: copyToClipboard
     * Description: copies contents of a given location to the clipboard
     * 
     * @since 2.0
     * 
     * @param   {string}    l   DOM id or class of location to copy from with # or . prepended
     * @param   {string}    b   DOM id of button with # prepended
     * @returns {boolean}   false to prevent button from navigating
     */
    this.copyToClipboard = function (l, b) {
        if (typeof l == "undefined") { return false; }
        if ($(l).length < 1) { console.log(l + "does not exist"); return false; }

        if (navigator.clipboard) {
            navigator.clipboard.writeText($(l).val()).then(
                function () { _me.flashCopySuccess(true); }
                ).catch(
                    function () { _me.flashCopySuccess(false); }
                );
        } else if (document.queryCommandSupported('copy') && document.queryCommandEnabled('copy'))  {
            $(l).select();
            _me.flashCopySuccess(document.execCommand('copy'));
            $(b).focus();
        } else {
            /* copy functions only work if you have https */
            if (window.location.href.indexOf('https') < 0) { console.log("No HTTPS, no copy!"); }
            _me.flashCopySuccess(false);
        }
        return false;  
    }

    /** 
     * Function: flashCopySuccess
     * Description: flashes a success message if copy to clipboard worked
     * 
     * @since 2.0
     * 
     * @param   {boolean}   success     whether to flash a success message or error message
     */
    this.flashCopySuccess = function (success) {
        if (success) {
            $("#copy_success").fadeIn();
            setTimeout(function() { $("#copy_success").fadeOut(); }, 2000);
        } else {
            $("#copy_fail").fadeIn();
            setTimeout(function() { $("#copy_fail").fadeOut(); }, 2000);
        }
    }

    /** 
     * Function: getAboutPg
     * Description: displays the about page with the appropriate section
     * 
     * @since 2.0
     * 
     * @param   {string}    p   page name to display
     * @returns {boolean}   false to prevent button from leaving page
     */
    this.getAboutPg = function (p) {
        var apgs = ["about_patterns", "about_custom_patterns", "about_dice", "about_rr"];
        if (typeof p != "string" && apgs.indexOf(p) < 0 && apgs != "close") { return false; }
        
        /* set about page to display */
        $(".aboutpg").hide();
        $("#" + p).show();
        $("#rr_body_right_col").scrollTo(0);

        /* make about page appear */
        if ($("#rr_body_right_col").css("display") == "none") {
            if($(window).width() < 768) {
                if (_me.activeScreen == "welcome") {
                    $("#rr_welcome").hide("slide", {direction: "left"}, 400, 
                    function () { $("#rr_body_right_col").show("slide", {direction: "right"}, 400); });
                } else {
                    $("#rr_form").hide("slide", {direction: "left"}, 400, 
                    function () { $("#rr_body_right_col").show("slide", {direction: "right"}, 400); });
                }
                
            } else {
                $("#rr_body_center_lt").hide("slide", {direction: "left"}, 200, 
                function () { $("#rr_body_right_col").fadeIn(400); });
            }
        } else if (p == "close") {
            if ($(window).width() < 768) {
                $("#rr_body_right_col").hide("slide", {direction: "right"}, 400,
                    function () {
                        if (_me.activeScreen == "welcome") {
                            $("#rr_welcome").show("slide", {direction: "left"}, 400);
                        } else {
                            $("#rr_form").show("slide", {direction: "left"}, 400);
                        }
                    });
            } else {
                $("#rr_body_right_col").fadeOut(400, 
                    function () { $("#rr_body_center_lt").show("slide", {direction: "left"}, 200); });
            }
        }

        /* let's not leave the main page */
        return false;
    }

    /**
     * Function: loadAllStrings
     * Description: loads all strings into interface
     * 
     * @since 2.0
     */
     this.loadAllStrings = function () {
        /* only execute if strings were actually loaded */
        if (_me.strings.length < 1) { 
            console.log("localization strings did not load.");
            /* display welcome page */
            $("#rr_start").fadeOut();
            return False;
        }

        var dataField = "";
        var itemToLocalize = {};
        for (var i=0; i < _me.toTranslate.length; i++) {
            itemToLocalize = $("#" + _me.toTranslate[i]);
            if (itemToLocalize.length > 0) {
                dataField = itemToLocalize.data("i18n");
                if (dataField != "undefined") {
                    itemToLocalize.html(_me.strings[dataField]);
                }
            }
        }
        /* link about page */
        $(".rr-about").on("click", function () { _me.getAboutPg($(this).data("aboutpg")) });

        /* display welcome page */
        $("#rr_start").fadeOut();
    }

    /**
     * Function: setMode
     * Description: sets the mode of Random Roller
     * 
     * @since 2.0
     * 
     * @param   {string}    m   mode to set ('dice' (default) or 'auto')
     * @returns {boolean}   false to prevent button from navigating away from page
     */
    this.setMode = function (m) {
        /* make sure data passed is valid */
        if (typeof m != "string" && m != "dice" && m != "auto") { m = "dice"; }

        switch (m) {
            case "auto":
                if ($("#dice_rolls").css("display") !="none") {
                    $("#dice_rolls").slideUp();
                }
                $("#dice_button").fadeOut(200, function () { $("#auto_button").fadeIn(); });
                $("#mode_auto").fadeOut(200, function () { $("#mode_dice").fadeIn(); });
                break;
            
            case "dice":
                if ($("#dice_rolls").css("display") == "none") {
                    $("#dice_rolls").slideDown();
                }
                $("#auto_button").fadeOut(200, function () { $("#dice_button").fadeIn(); });
                $("#mode_dice").fadeOut(200, function () { $("#mode_auto").fadeIn(); });
                $("#pattern_select").trigger("change");
                break;

            default:
                return false;
        }
        
        if ($("#rr_form").css("display") == "none") {
            $("#rr_welcome").fadeOut(200,
                function () { $("#rr_form").fadeIn(200); }    
            );   
            _me.activeScreen = "form";
        } 
        return false;
    }

    /** 
     * Function: viewportSize
     * Description: resizes the viewport
     * 
     * @since 2.0
     */
    this.viewportSize = function () {
    var b = $("#rr_body");
    var layers = [$("#rr_window"), $("#rr_title"), $("#rr_footer")];
    var h = 0;
    var heights = [];
    b.height(100);
    for (var i=0; i<layers.length; i++) {
        h = layers[i].height();
        h += parseInt(layers[i].css("padding-top"));
        h += parseInt(layers[i].css("padding-bottom"));
        heights.push(h);
    }
    var bh = heights[0] - heights[1] - heights[2];
    bh -= parseInt(b.css("padding-top"));
    bh -= parseInt(b.css("padding-bottom"));
    b.height(bh);
    $(".rr-body").height(bh);
}
    


    /* object initialization */
    _me._init();

}
