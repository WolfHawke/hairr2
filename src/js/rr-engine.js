/**
 * Hawke AI Random Roller
 * JavaScript engine
 *
 * @ver 2.0
 * @author JMW
 */

 /**
  * Object: RR
  * Description: object to run the Random Roller app.
  * Arguments: selfRef = id used in the window object to refer to the wizard; required
  *            libLang = variable to define a different language Diceware library; optional; defaults to "en"
  * Returns: nothing
  * 
  * @since 2.0
  */
function RR (selfRef, libLang) {
    if (typeof selfRef == "undefined") { 
        selfRef = false; 
    } else if (selfRef.length < 1 || $("#" + selfRef).length < 1) {
        selfRef = false;
    }
    /* if we don't have a libLang explicitly defined, use the language attribute in the html tag */
    if (typeof libLang == "undefined") { 
        libLang = $("html").attr("lang")
        if (typeof libLang == "undefined" || (libLang.length != 2 && libLang.length != 5)) {
         libLang = "en";   
        };
    }

    console.log("Diceware word list language:", libLang);

    /* object properties */
    this.selfRef = selfRef;
    this._dwLibLang = libLang;
    this.dw = {}; /* Diceware words object */
    this.da = {}; /* Diceware all characters (A-Z 0-9 etc.) object */
    this.dc = {}; /* Diceware chars and numbers object */
    this.dl = {}; /* Diceware letters and numbers object */
    this.ds = {}; /* Diceware special chars object */
    this.dq = {}; /* Diceware object chars, numbers and space object */
    this.drolls = {}; /* Number of rolls needed for each item */
    this.minchars = 14; /* Minimum number of characters in Diceword patterns */
    this._errors = []; /* thrown errors object */
    this.patterns = {}; /* built-in patterns */
    this.rollsToDo = 0; /* how many rolls are necessary to complete password */
    this.activePattern = []; /* array of patterns to roll manually */
    this.activeRoll = 0; /* which roll we are on in pattern */
    this.dwCaps = true; /* whether or not to capitalize Diceware words */

    /* object internal selfRef */
    var _me = this;

    /* object methods */
    /*
     * Function: _init
     * Description: initialization function for the script; Loads the diceware libraries
     * Arguments: none
     * Returns: nothing
     *
     * @since: 2.0
     */
    this._init = function() {
        /* load diceware library */
        var dwFile = "diceware/dw-" + _me._dwLibLang + ".json"

        $.getJSON(dwFile, function(d) { _me.dw = d; }
        ).fail(function(d) {
            $.getJSON("diceware/dw-en.json", function (d) { _me.dw = d; }
            ).fail(function(d){
                console.log("Unable to retrieve Diceware JSON", d);
                _me._errors.push("dwLib:diceware/dw-" + _me._dwLibLang + ".json");
            });
        });

        /* load all characters library */
        $.getJSON("diceware/da.json", function(d) { _me.da = d;
        }).fail(function(d) {
            console.log("Unable to retrieve Diceware JSON", d);
            _me._errors.push("dwLib:diceware/da.json");
        });

        /* load chars and digits library */
        $.getJSON("diceware/dc.json", function(d) { _me.dc = d;
        }).fail(function(d) {
            console.log("Unable to retrieve Diceware JSON", d);
            _me._errors.push("dwLib:diceware/dc.json");
        });

        /* load chars and digits library */
        $.getJSON("diceware/dl.json", function(d) { _me.dl = d;
        }).fail(function(d) {
            console.log("Unable to retrieve Diceware JSON", d);
            _me._errors.push("dwLib:diceware/dc.json");
        });

        /* load special chars */
        $.getJSON("diceware/ds.json", function (d) { _me.ds = d; }
        ).fail(function(d) {
            console.log("Unable to retrieve Diceware JSON", d);
            _me._errors.push("dwLib:diceware/ds.json");
        });
        /* load chars, digits and space */
        $.getJSON("diceware/dq.json", function(d) { _me.dq = d;
        }).fail(function (d) {
            console.log("Unable to retrieve Diceware JSON", d);
            _me._errors.push("dwLib:diceware/dq.json");
        });

        _me.drolls = {'w':5, 'a':3, 'c':2, 'l':3, 'q':3, 's':2, 'd':2, '_':0 }

        _me.patterns = { 'enhanced': 'c[wq]', 'classic': '[wc]', 'character': 'a',
                         'alphanumeric': 'l', 'decimal': 'd' }

        /* link buttons */
        $("#auto_button button").click(_me.doAutoRoll); 
        $("#clear_button_btn").click(_me.clearPasshprase);
        $("#dice_button button").click(_me.doManualRoll);

        /* select box change */
        $("#pattern_select").on("change", _me.selectPatternControl);
        $("#custom_pattern").on("blur", _me.checkCustomPattern);

        /* check for proper number of minimum characters */
        $("#minchar").on("blur", _me.checkMinChars);

        /* dice field change event */
        $(".rr-dice-roll").on("keyup", function() { 
            _me.rollEntered($(this)[0].id);
         });
    }

    /**
     * Function: activateCopyClear
     * Description: activates the Copy and Clear buttons
     * Arguments: which = string; which buttons to activate 'copy, 'clear' or 'both'
     *            active = boolean; whether to activate or deactivate. Defaults to true.
     * Returns: nothing
     */
    this.activateCopyClear = function(which, active) {
        if (typeof which == "undefined") { return false; }
        if (typeof active != "boolean") { active = true; }

        var btns = { 'copy': $("#copy_button_btn"), 'clear': $("#clear_button_btn") }
        var btnmod = ['copy', 'clear'];
        var i=0;

        if (which == 'copy' || which == 'clear') {
            btnmod = [which]
        }

        if (active) {
            for (i=0; i<btnmod.length; i++) {
                if (btns[btnmod[i]].prop("disabled") == true) {
                    btns[btnmod[i]].prop("disabled", false);
                }
            }
        } else {
            for (i=0; i<btnmod.length; i++) {
                if (btns[btnmod[i]].prop("disabled") == false) {
                    btns[btnmod[i]].prop("disabled", true);
                }
            }            
        }
    }

    /**
     * Function: autoRoll
     * Description: generates a diceware password using the random engine rather than real dice
     * Arguments: p = pattern
     *            ml = minimum number of characters (-1 = undefined)
     * Returns: password string
     * 
     * @since 2.0
     */
    this.autoRoll = function (p, ml) {
        if (typeof p != 'string') { return false; }
        if (typeof ml != 'number') { ml = -1; } /* default to undefined length */

        var pattern = _me.parsePattern(p);
        var out = false;
        var i = 0;
        var ri = 0; /* roll iterator */
        var rs = ""; /* roll string */

        if (pattern !== false && pattern.length > 0) {
            out = "";
            while(out.length < ml || ml == -1) {
                for (i=0; i<pattern.length; i++) {
                    rs = "";
                    for(ri=0; ri<_me.drolls[pattern[i]]; ri++) {
                        rs += _me.rndRoll().toString();
                    }
                    out += this.getRollResult(pattern[i], rs);
                }
                if (ml < 1) { break; } /* only iterate through the pattern once */
                if (out.length > 256) { break; } /* protect against runaway passwords */
            }
        } 
        return out;
    }


    /**
     * @function capitalizeString
     * Description: capitalizes random characters in a string
     * 
     * @param   {String}    r   5-dice roll for word to capitalize
     * @param   {string}    s   string to capitalize
     * @returns {string}
     */
    this.capitalizeString = function(r, s) {
        var rolls = {"1":parseInt(r.substr(0,1)), "2":parseInt(r.substr(1,1)), "3":parseInt(r.substr(2,1)),
                     "4":parseInt(r.substr(3,1)), "5":parseInt(r.substr(4,1))};
        var maxCaps = s.length-1;
        var ucString = s;
        var tmpStr = "";
        var caps = 1;
        var i = 0;
        var j = 0;
        var ucChar = 0;
        
        /* lower case */
        if ((rolls["2"] == 4 || rolls["2"] == 5) && (rolls["4"] == 1 || rolls["4"] == 5 || rolls["4"] == 6)) {
            return s;
        } else if ((rolls["2"] == 1 || rolls["2"] == 6) && (rolls["4"] == 3 || rolls["4"] == 4 || rolls["4"] == 6)) {
            return s.toUpperCase();
        } else {
            /* determine number of letters to capitalize */
            if (rolls["1"] > maxCaps) {
                if (rolls["3"] > maxCaps) {
                    if (rolls["5"] > maxCaps) {
                        caps = 1; /* we default to one if we have too few characters in the string */
                    } else {
                        caps = rolls["5"];
                    }
                } else {
                    caps = rolls["3"];
                }
            } else {
                caps = rolls["1"];
            }

            for(i=1; i<=maxCaps; i++) {
                tmpStr = "";
                ucChar = rolls[i.toString()];
                if (ucChar > ucString.length) {
                    ucChar = ucChar - ucString.length;
                } 
                ucChar--; /* because JavaScript runs from 0 */
                for (j=0; j<ucString.length; j++) {
                    if (j == ucChar) {
                        tmpStr += ucString.substr(j,1).toUpperCase();
                    } else {
                        tmpStr += ucString.substr(j,1);
                    }
                }
                ucString = tmpStr;
            }

            return ucString;
        }

    }


    /**
     * @function checkCustomPattern
     * Description: Checks to make sure that the custom pattern is valid
     * Arguments: none
     * Returns: nothing
     * 
     * @since 2.0
     */
    this.checkCustomPattern = function () {
        var p = _me.parsePattern($("#custom_pattern").val());
        if ($.isArray(p)) {
            $("#custom_pattern").addClass("is-valid");
            _me.setDiceNumber(p[0]);
            $("#roll_one").focus();
            _me.setNumOfRolls();
            _me.displayNumRolls();
        } else {
            $("#custom_pattern").addClass("is-invalid");
            $("#custom_pattern").focus();
        }
    }

    /**
     * Function: checkMinChars
     * Description: checks the #minchar field to see if length is in specified parameters; 
     *              updates it accordingly; minumum 4 for decimals, 8 for others; maximum 256
     * Arguments: none
     * Returns: boolean denoting result
     * 
     * @since 2.0
     */
    this.checkMinChars = function () {
        var minchars = parseInt($("#minchar").val());
        var minval = ($("#pattern_select").val() == "decimal") ? 4 : 8;
        var okay = true;
        
        /* check for minimum */
        if (isNaN(minchars) || minchars < minval) {
            minchars = minval;
            okay = false;
        }
        
        /* check for maximum = 256 */
        if (minchars > 256) { 
            minchars = 256;
            okay = false;
        }

        /* set minchar field */
        if (!okay) { $("#minchar").val(minchars); }
        _me.setNumOfRolls();
        _me.displayNumRolls();

        return okay;
    }

    /** 
     * Function: clearPassphrase
     * Description: clears the passphrase and resets the interface
     * Arguments: none
     * Returns: false so button does not navigate
     * 
     * @since 2.0
     */
    this.clearPasshprase = function() {
        $("#rr_pw").val("");
        _me.activateCopyClear('both', false);
        $("#custom_pattern").removeClass("is-valid", "is-invalid");
        if ($("#dice_button").css("display") != "none") {
            ["#pattern_select", "#minchar", "#custom_pattern", 
            ".rr-dice-roll", "#dice_button button"].forEach(
                function (i) { $(i).prop("disabled", false); });
            $("#roll_one").focus();
            _me.setNumOfRolls();
            _me.displayNumRolls();
        } else {
            $("#dice_button button").prop("disabled", false);
            $("#auto_button button").focus();
        }
    
    }

    /**
     * Function: displayNumRolls
     * Description: displays the number of rolls needed text
     * Arguments: none
     * Returns: nothing
     * 
     * @since 2.0
     */
    this.displayNumRolls = function () {
        var msg = "";

        if ($("#pattern_select").val() == 'enhanced' || $("#pattern_select").val() == 'classic') {
            $("#how_many_rolls")
            msg = $("#rr_dynamic_estimated_rolls").html();    
        } else {
            msg = $("#rr_dynamic_actual_rolls").html();
        }
        $("#how_many_rolls").html(msg.replace("%d", _me.rollsToDo));
    }

    /** 
     * Function: doAutoRoll
     * Description: triggers an automatic roll from the interface
     * Arguments: none
     * Returns: false to prevent button from navigating away
     * 
     * @since 2.0
     */
    this.doAutoRoll = function () {
        var passphrase = "";
        var selected = $("#pattern_select").val();
        var pattern = "";
        var minchars = parseInt($("#minchar").val());
        
        if (selected == "custom") {
            pattern = $("#custom_pattern").val();
            minchars = -1;
        } else {
            pattern = _me.patterns[selected];
        }

        if (selected == "enhanced") {
            minchars--;
            passphrase = _me.autoRoll(pattern.substr(0,1), -1);
            passphrase += _me.autoRoll(pattern.substr(1), minchars);
        } else {
            passphrase = _me.autoRoll(pattern, minchars);
        }

        $("#rr_pw").val(passphrase);

        _me.activateCopyClear('both', true);
        /* make sure that we cannot create passwords in manual mode if a password exists */
        $("#dice_button button").prop("disabled", true);

        return false;
    }

    /**
     * Function: doManualRoll
     * Descriptions: begins a manual roll sequence
     * Arguments: none
     * Returns: false to prevent button from navigating
     * 
     * @since: 2.0
     */
    this.doManualRoll = function () {
        var passphrase = $("#rr_pw").val();
        var selected = $("#pattern_select").val();
        var pattern = "";
        var minchars = parseInt($("#minchar").val());
        var complete = false;
        var rollConcat = "";
        var i = 0;
        var numberTxt = {"1": "one", "2": "two", "3": "three", "4": "four", "5": "five"}

        
        /* set button enablement */
        if ($("#rr_pw").val().length < 1){
            $("#pattern_select").attr("disabled", true);
            $("#minchar").attr("disabled", true);
            if ($("#custom_pattern").css("display") != "none") {
                $("#custom_pattern").attr("disabled", true);
            }
            _me.activateCopyClear("clear", true);
            if (selected == "character" || selected == "alphanumeric" || selected == "decimal") {
                pattern = "[" + _me.patterns[selected] + "]=" + minchars;
            } else if (selected == "custom") {
                pattern = $("#custom_pattern").val();
            } else {
                pattern = _me.patterns[selected];
            }
            _me.activePattern = _me.parsePattern(pattern);
            console.log(pattern, _me.activePattern);
            _me.activeRoll = 0;
        }

        /* concatenate rolls */
        for (i=1; i<=_me.drolls[_me.activePattern[_me.activeRoll]]; i++) {
            rollConcat += $("#roll_" + numberTxt[i.toString()]).val();
        }

        /* generate next item in passphrase */
        passphrase += _me.getRollResult(_me.activePattern[_me.activeRoll], rollConcat);

        /* increment step */
        $("#rr_pw").val(passphrase);
        _me.rollsToDo--;
        _me.displayNumRolls();
        _me.activeRoll++;
        
        /* check for stopping point */
        if (_me.activeRoll >= _me.activePattern.length){
            if (passphrase.length >= minchars) {
                complete = true;
            } else if ((selected == "enhanced" || selected == "classic")) {
                /* enhanced and classic will most likely not complete on first pass */
                _me.activeRoll = (selected == "enhanced") ? 1 : 0;
                _me.rollsToDo = (_me.rollsToDo < 1) ? 2 : _me.rollsToDo;
            }
        } 
        if (!complete){
            _me.setDiceNumber(_me.activePattern[_me.activeRoll]);
            $("#roll_one").focus();
        } else {
            _me.setDiceNumber(_me.activePattern[0]);
            $(".rr-dice-roll").attr("disabled", true);
            $("#dice_button button").attr("disabled", true);
            _me.activateCopyClear("both", true);
            $("#how_many_rolls").html($("rr_dynamic_rolls_complete").html());
        }
        $(".rr-dice-roll").val("");

        return false;
    }

    /**
     * Function: getRollResult
     * Description: gets a roll result from a given library
     * Argumets: l = library to get from [acdlqsw_]
     *           r = roll to get
     *           c = capitalize words randomly (default = True)
     * Returns: string containing result; false on error
     * 
     * @since 2.0
     */
    this.getRollResult = function(l,r,c) {
        /* check for proper characters in level */
        if (typeof l != 'string' || !$.isArray(l.match(/^[acdlqsw_]$/))) { 
            console.log('getRollResult: invalid library: ' + l);
            return false; 
        } else if (l == "_") { /* space doesn't need a roll, so just return it */
            return " "; 
        }
        /* check for proper characters in roll */
        if (typeof r != 'string' || !$.isArray(r.match(/^[1-6]{2,6}$/))) { 
            console.log('getRollResult: roll is not a string: ' + r)
            return false; 
        }
        if (typeof c != 'boolean') { c = true; }
        
        /* check for proper length of roll */
        if (r.length != _me.drolls[l]) { return false; }

        var out = "";
        var firstRoll = "";
        var secondRoll = "";
        var thirdRoll = "";
        var first = 0;
        var second = 0;

        switch (l) {
            case 'a':   /* any ascii character */
                /* split rolls */
                firstRoll = r.substr(0,1);
                secondRoll = r.substr(1,1);
                thirdRoll = r.substr(2,1);

                switch (firstRoll) {
                    case "1":
                    case "2":
                        first = _me.da.onetwo;
                        break;
                    case "3":
                    case "4":
                        first = _me.da.threefour;
                        break;
                    case "5":
                    case "6":
                        first = _me.da.fivesix;
                }
                second = first[parseInt(secondRoll)-1];
                out = second[parseInt(thirdRoll)-1];
                break;
            case 'c':   /* any special character or digit */
                out = _me.dc[parseInt(r)];
                break;
            case 'd':   /* any digit (0-9) */
                firstRoll = parseInt(r.substr(0,1));
                secondRoll = parseInt(r.substr(1,1));

                /* if we roll a 6, we get a random number between 1 and 5 */
                if (firstRoll > 5) {
                    while (firstRoll < 1 || firstRoll > 5) {
                        firstRoll = Math.floor(Math.random() * 10);
                    }
                }

                if (secondRoll == 2 || secondRoll == 4 || secondRoll == 6) {
                    if (firstRoll + 5 == 10) {
                        out = 0;
                    } else {
                        out = firstRoll + 5;
                    }
                } else {
                    out = firstRoll;
                }
                out = out.toString();
                break;
            case 'l':
                firstRoll = r.substr(0,1);
                secondRoll = r.substr(1,1);
                thirdRoll = r.substr(2,1);
                out = _me.dl[parseInt(firstRoll)-1][parseInt(secondRoll)-1];
                if (thirdRoll == 2 || thirdRoll == 4 || thirdRoll == 6) {
                    out = out.toLowerCase();
                }
                break;
            case 'q':   /* any special character or digit and space */
                out = _me.dq[parseInt(r)];
                break;
            case 's':   /* any special character */
                out = _me.ds[parseInt(r)];
                break;
            case 'w':   /* diceware word */
                var w = _me.dw[parseInt(r)];
                if (_me.dwCaps) {
                    /* create random roll for now */
                    var tmpRoll = "";
                    for (var i=0; i<6; i++) {
                        tmpRoll += _me.rndRoll().toString();
                    }
                    out = _me.capitalizeString(tmpRoll, w);
                } else {
                    out = w;
                }
                break;
            default:
                console.log("getRollResult: switch command failed");
                out = false;
                break;
        }
        return out;
    }

    /**
     * Function: parsePattern
     * Description: parses password generation pattern and returns an array containing the array
     * Arguments: p = string : pattern to parse 
     * Returns: Array or False if invalid string
     * 
     * @since 2.0
     */
    this.parsePattern = function (p) {
        /* make sure we're dealing with a string */
        if (typeof p != 'string') { return false; }
    
        /* check pattern for valid characters */
        var validChars = /^[\[\]acdlqsw_\=0-9]+$/;
        if (!$.isArray(p.match(validChars))) { return false; }
    
        /* iterate through string */
        var i = 0;
        var j = 0;
        var k = 0;
        var l = 0;
        var repeat = 0;
        var sp = "";
        var out = [];
        while (i<p.length) {
          if (p.substr(i,1) == "[") {
              sp = "";
              j = i;
              while (j<p.length) {
                  if (p.substr(j,1) == ']') {
                      sp = p.substr((i+1),(j-i-1));
                      if (p.substr(j+1,1) == '=' ) {
                            repeat = "";
                            k = j+2; 
                            while (!isNaN(parseInt(p.substr(k,1))) && k < p.length) {
                                repeat += p.substr(k,1);
                                k++;
                            }
                            repeat = parseInt(repeat);
                            j = k;
                        } else {
                            repeat = 1;
                        }
                        for (k=0; k<repeat; k++) {
                            l = 0;
                            while (l<sp.length) {
                                out.push(sp.substr(l,1))
                                l++;
                            }
                        }
                        break; /* out of while loop back to next character */
                  }
                  j++;
              }
              i = j;
          } else {
                    if ($.isArray(p.substr(i,1).match(/[acdlqsw_]/))){
                        out.push(p.substr(i,1));
                    }
                i++;
          }
        }
    
        return out;
    }

    /**
     * Function: rndRoll
     * Description: Approximates a randomized dice roll
     * Arguments: none
     * Returns: number between 1 and 6
     * 
     * @since 2.0
     */
    this.rndRoll = function () {
        var roll = 0;
        while (roll < 1 || roll > 6 ) {
            roll = Math.floor(Math.random() * 10);
        }
        return roll;
    }

    /**
     * Function: rollEntered
     * Description: checks roll validity and moves on to next field
     * Arguments: id = layer id to work on
     * Returns: nothing
     */
    this.rollEntered = function (id) {
        var r = $("#" + id);
        var rv = parseInt(r.val());
        r.removeClass("roll-is-invalid");
        if (rv >= 1 && rv <= 6) {
            switch(id) {
                case "roll_one":
                    $("#roll_two").focus();
                    break;
                case "roll_two":
                    if ($("#dice_three_col").css("display") != "none") {
                        $("#roll_three").focus();
                    } else {
                        $("#dice_button button").focus();
                    }
                    break;
                case "roll_three":
                    if ($("#dice_four_col").css("display") != "none") {
                        $("#roll_four").focus();
                    } else {
                        $("#dice_button button").focus();
                    }
                    break;
                case "roll_four":
                    if ($("#dice_five_col").css("display") != "none") {
                        $("#roll_five").focus();
                    } else {
                        $("#dice_button button").focus();
                    }
                    break;
                case "roll_five":
                    $("#dice_button button").focus();
            }
        } else {
            r.addClass("roll-is-invalid");
            r.focus();
        }
    }

    /** 
     * Function: selectPatternControl
     * Description: controls the select box for the pattern control
     * Arguments: none
     * Returns: nothing
     */
    this.selectPatternControl = function () {
        if ($("#pattern_select").val() == "custom") {
            $("#minchar").attr("disabled", true);
            if ($("#cpattern").css("display") == "none") {
                $("#cpattern").slideDown();
            }
            $("#custom_pattern").focus();
            $("#custom_pattern").select();
            _me.setDiceNumber("w"); /* turn on all five until we lose focus on custom */
        } else {
            $("#minchar").attr("disabled", false);
            if ($("#cpattern").css("display") != "none") {
                $("#cpattern").slideUp();
            }
            $("#minchar").focus();
            $("#minchar").select();
            /* set dice roll boxes */
            var p = _me.patterns[$("#pattern_select").val()];
            if (p.substr(0,1) == '[') { p = p.substr(1); }
            _me.setDiceNumber(p.substr(0,1));
        }
        _me.setNumOfRolls();
        _me.displayNumRolls();
    }

    /**
     * Function: setDiceNumber
     * Description: sets how many dice fields are necessary for a roll
     * Arguments: p = string; pattern code (one letter) to set number of dice
     * Returns: nothing
     * 
     * @since 2.0
     */
    this.setDiceNumber = function (p) {
        if (typeof p == "undefined") { return false; }

        var colTxtNums = { "1": "one", "2": "two", "3": "three", "4": "four", "5": "five" }

        $(".dice-roll-col").hide();

        if (p == "_") {
            $("#dice_space").show();
        } else {
            if ($("#dice_space").css("display") != "none") {
                $("#dice_space").hide();
            }
            for (var i=1; i<=_me.drolls[p]; i++) {
                $("#dice_" + colTxtNums[i.toString()] + "_col").show();
            }
        }
    }

    /**
     * Function: setNumOfRolls
     * Description: sets the number of rolls necessary to create pattern
     * Arguments: none
     * Returns: nothing
     * 
     * @since 2.0
     */
    this.setNumOfRolls = function () {
        var minchars = parseInt($("#minchar").val());
        var actchars = 0;
        _me.rollsToDo = 0;

        switch($("#pattern_select").val()) {
            case 'enhanced':
                _me.rollsToDo = 1;
                actchars = 1;
            case 'classic':
                while (actchars < minchars) {
                    actchars += 4; /* diceword */
                    _me.rollsToDo++;
                    actchars++;    /* in between character */
                    _me.rollsToDo++;
                }
                break;
            case 'custom':
                _me.rollsToDo = _me.parsePattern($("#custom_pattern").val()).length;
                break;
            default:
                _me.rollsToDo = minchars;
        }
    }

    /* initialize object data upon instantiation */
    _me._init();
}
