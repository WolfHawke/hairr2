/**
 * Hawke AI Random Roller
 * JavaScript PWA functions
 * 
 * Last update: 2021-09-09
 * 
 * @ver 2.0
 * @author JMW
 * 
 * PWA scripts adapted from 
 * _Progressive Web Apps_ by David Alan Hume 
 * (Shelter Island, NI: Manning Publications Co., 2018)
 * 
 * Required to be in root so that all the other fetching works
 */

"use strict";

var cacheName = "rr:2021-09-09:4";

var items = [
    "index.html",
    "js/jquery-3.5.1.min.js",
    "js/jquery-ui.min.js",
    "js/jquery.scrollTo.min.js",
    "js/bootstrap.min.js",
    "js/fontawesome.min.js",
    "js/rr-engine.js",
    "js/rr-interface.js",
    "css/bootstrap.min.css",
    "css/fontawesome/all.min.css",
    "css/rr.css",
    "css/rr-dark-mode.css",
    "diceware/da.json",
    "diceware/dc.json",
    "diceware/dl.json",
    "diceware/dq.json",
    "diceware/ds.json",
    "diceware/dw-en.json",
    "i18n/en.json",
    "img/hairr-2-logo.svg"
];


/* installation */
self.addEventListener("install", e => {
    e.waitUntil(caches.open(cacheName).then(cache => cache.addAll(items)));
});


/* fetch from cache */
self.addEventListener("fetch", function (event) {
    event.respondWith(caches.match(event.request).then(
        function (response) {
            if (response) { return response; }
            
            var requestToCache = event.request.clone();
            
            return fetch(requestToCache).then(
                function (response) {
                    if (!response || response.status !== 200) {
                        return response;
                    }

                    var responseToCache = response.clone();

                    caches.open(cacheName).then(
                        function (cache) {
                            cache.put(requestToCache, responseToCache);
                        }
                    );

                    return response;
                }
            );
        }   
    ));
});

/* activate */
self.addEventListener('activate', (e) => {
    e.waitUntil(caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
            if (key === cacheName) { return; }
            return caches.delete(key);
        }))
    }));
});
