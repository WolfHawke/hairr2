# generate  a list of codes for a given number of dice
dice = 3
if dice > 5:
    print("Max five dice!")
    exit()
dl = []
codes = []
for x in range(0, dice):
    dl.append(1);

while len(codes) < (6**dice):
    item = ""
    for x in range(0,dice):
        item = item + str(dl[x])
    codes.append(item)
    dl[dice-1] = dl[dice-1] + 1
    if dl[dice-1] > 6:
        dl[dice-1] = 1
        if (dice > 1):
            dl[dice-2] = dl[dice-2] + 1
            if dl[dice-2] > 6:
                dl[dice-2] = 1
                if (dice > 2):
                    dl[dice-3] = dl[dice-3] + 1
                    if dl[dice-3] > 6:
                        dl[dice-3] = 1
                        if (dice > 3):
                            dl[dice-4] = dl[dice-4] +1
                            if dl[dice-4] > 6:
                                dl[dice-4] = 1
                                if (dice > 4):
                                    dl[dice-5] = dl[dice-5] + 1
                                    if dl[dice-5] > 6:
                                        dl[dice-5] = 1

char = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", 
        "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", 
        "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

c = 0;

out = "{";
for x in codes:
    if x == 665 or x == 666:
        c = len(char) - 1
    out = out + "\"" + x + "\" : \"" + char[c] + "\","
    c = c + 1
    if c == len(char):
        c = 0
out = out[:-1] + "}\n";

with open('codes.json','w') as f:
    f.write(out)
